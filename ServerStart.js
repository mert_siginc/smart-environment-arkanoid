var app = require('express')();
var http = require('http').Server(app);
var io = require('socket.io')(http);
var monitorOnline = false;
var userOnline = false;

var port = 3000;
var auth= "1234";

var ball_X = 0;
var ball_Y = 0;
var paddle_X = 0;



var cycle = setInterval(function () {

	
	if(counter % 3 == 0){
		
		

	}
	
	counter++;
  }, 1000 / 60);
  
   var ip = req.headers['x-forwarded-for'] || 
     req.connection.remoteAddress || 
     req.socket.remoteAddress ||
     req.connection.socket.remoteAddress;
 console.log(ip);



app.get('/getAuth', function (req, res) {
	res.send(JSON.stringify(auth));
});

app.get('/setPaddlePosition/:x/:auth', function (req, res) {
	if(req.params.auth == auth){
		paddle_X = req.params.x;   
		console.log('Paddle Position: x= ' + paddle_X);
		res.status(200).send(JSON.stringify('OK'));
	}
	else{	
		console.log('unautherisierter Zugriff');	
		res.status(403).send(JSON.stringify('You have no Rights!'));
	}
});

app.get('/setBallPosition/:x/:y/:auth', function (req, res) {
	if(req.params.auth == auth){
		ball_X = req.params.x;
		ball_Y = req.params.y;		
		console.log('Ball Position: x= ' + ball_X + '  y= ' + ball_Y);    
		res.status(200).send(JSON.stringify('OK'));
	}
	else{	
		console.log('unautherisierter Zugriff');	
		res.status(403).send(JSON.stringify('You have no Rights!'));
	}
});

app.get('/getPaddlePosition', function (req, res) {
   res.send(JSON.stringify(paddle_X.toString()));
});



app.get('/getBallPosition', function (req, res) {
	var returnXY =  Array(ball_X, ball_Y);
	res.send(JSON.stringify(returnXY));	
});

app.listen(port, function(){
  console.log('listening on *: '+port);
});