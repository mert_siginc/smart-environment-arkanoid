function GetBallCoordinates(s) {
    
    this.s = s;
    
    this.tpe = "GetBallCoordinates";
}

function GetPaddleCoordinates(s) {
    
    this.s = s;
    
    this.tpe = "GetPaddleCoordinates";
}

function SetPaddleCoordinates(s) {
    
    this.s = s;
    
    this.tpe = "SetPaddleCoordinates";
}

function SetBallCoordinates(s) {
    
    this.s = s;
    
    this.tpe = "SetBallCoordinates";
}




var EchoDeserializer = new function() {
	var transform = function(data, deser) {
    	var re = {};
    	Object.keys(data).forEach(function(k) {
    	    var value = data[k];

			if (Array.isArray(value)) {
            	value = data[k].map(function(val) {
                	console.log(val);
                	if(typeof val === 'object')
                    	return deser.deserialize(val);
                	else return val;
            	});
        	} else if(typeof value === 'object') {
    	        value = deser.deserialize(value);
   		    }
        	re[k] = {
            	writable: false,
            	configurable: false,
            	value: value
        	};
    	});
    	return re;
	}

    this.deserialize = function(json) {
        
        if(json.tpe === "GetBallCoordinates")
            return this.deserializeGetBallCoordinates(json);
        
        if(json.tpe === "GetPaddleCoordinates")
            return this.deserializeGetPaddleCoordinates(json);
		
		if(json.tpe === "SetPaddleCoordinates")
            return this.deserializeSetPaddleCoordinates(json);
		
		if(json.tpe === "SetBallCoordinates")
            return this.deserializeSetBallCoordinates(json);
        
    };

    
    this.deserializeGetBallCoordinates = function (data) {
        return Object.create(GetBallCoordinates.prototype, transform(data, this));
    };
    
    this.deserializeGetPaddleCoordinates = function (data) {
        return Object.create(GetPaddleCoordinates.prototype, transform(data, this));
    };
	
	 this.deserializeSetPaddleCoordinates = function (data) {
        return Object.create(SetPaddleCoordinates.prototype, transform(data, this));
    };
	
	 this.deserializeSetBallCoordinates = function (data) {
        return Object.create(SetBallCoordinates.prototype, transform(data, this));
    };
    
};